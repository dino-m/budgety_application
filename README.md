#Budgety app

JS application for calculating budget income and expenses.

###DESCRIPTION:

- Application display's currently month and year.
- User needs to pick if it is income or expense (+ or -).
- After selection income or expense option he needs to insert some description.
- Finally user needs to insert some value and press ENTER or click on button.
- Budgety will calculate and display all of incomes and expenses and their sum.
- Beside the budget values, app calculate and display's percentages of expenses.